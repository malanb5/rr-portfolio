import { combineReducers } from 'redux';
import storyReducer from './story';
import archiveReducer from './archive';
import headerReducer from './header';

const rootReducer = combineReducers({
  storyState: storyReducer,
  archiveState: archiveReducer,
  headerState : headerReducer,
});

export default rootReducer;