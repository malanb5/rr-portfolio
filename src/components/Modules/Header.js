import React from 'react';
import { connect } from 'react-redux';

import mapStateToProps from "react-redux/lib/connect/mapStateToProps";

import "./Header.css";

const LOGO_CONTENT = {
    name : "Matthew Bladek",
};

const COLUMNS = {
    CV: {
        label: 'CV',
        route: '/',
        width: '10%',
    },
    linkdin: {
        label: 'Linkedin',
        route: 'https://www.linkedin.com/in/matthew-bladek-b58702170/',
        width: '10%',
    },
    github: {
        label: 'Github',
        route: 'http://github.com/malanb5',
        width: '10%',
    },
    comments: {
        label: 'Projects',
        route: '/',
        width: '10%',
    },
    contact: {
        label: 'Contact',
        route: '/',
        width: '10%',
    },
};

const LogoHeader = ({content}) => (
    <span className={"logo_header"}>
    {content["name"]}
    </span>
);


const MenuItems = ({}) =>
    <div className = "header">
        <Header columns={COLUMNS} />
    </div>;

const Header = ({ columns }) =>
    <div className={"header"}>
        <LogoHeader content={LOGO_CONTENT}/>
        {Object.keys(columns).map(key =>
            <span
                key={key}
                style={{ width: columns[key].width }}
            >
                <a className="menuItemLinks" href={columns[key].route}>{columns[key].label}</a>
            </span>
        )}
    </div>;

export default MenuItems;