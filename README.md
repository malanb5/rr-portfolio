<h1>Installation</h1>

<ul>
<li/>`git clone git@github.com:malanb5/react-redux-newsfeed`
<li/>`cd react-redux-tutorial`
<li/>`npm install`
<li/>`npm start`
<li/>visit http://localhost:3000/
</ul>

<p>
Based upon the tutorial by Robin Wieruch @ https://www.robinwieruch.de/
</p>
