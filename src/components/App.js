import React from 'react';
import './App.css';
import Stories from './Stories/Stories';
import SearchStories from './Modules/SearchStories';
import Header from "./Modules/Header"

const App = () =>
    <div className="app">
        <Header/>
        <div className="interactions">
            <SearchStories />
        </div>
        <Stories />
    </div>

export default App;