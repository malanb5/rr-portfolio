import { HEADER_ADD } from '../constants/actionTypes';

const INITIAL_STATE = {
    menuItems: [],
    error: null,
};

const applyAddMenuItem = (state, action) => ({
    menuItems: action.menuItems,
    error: null,
});

function headerReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        case HEADER_ADD : {
            return applyAddMenuItem(state, action);
        }
        default : return state;
    }
}

export default headerReducer;