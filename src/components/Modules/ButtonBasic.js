import React from 'react';

const ButtonInline = ({
  onClick,
  type = 'button',
  children
}) =>
  <ButtonBasic primary
               type={type}
               className="button-inline"
               onClick={onClick}
  >
    {children}
  </ButtonBasic>

const ButtonBasic = ({
  onClick,
  className,
  type = 'button',
  children
}) =>(
  <span>

    <button
    type={type}
    className={className}
    onClick={onClick}
  >
    {children}
  </button>

  </span>
);

export default ButtonBasic;

export {
  ButtonInline
};